from django.contrib.auth.decorators import login_required, permission_required
from django.urls import path

from .feeds import UpcomingTripsFeed
from .views import TripsListView, TripDetailView, TripCreateView, TripUpdateView, TripDeleteView, TripSignUpView, TripBailView

app_name = 'trips'

urlpatterns = [
    path('', TripsListView.as_view(), name='index'),
    path('feed/', UpcomingTripsFeed()),
    path('<int:pk>/', TripDetailView.as_view(), name='detail'),
    path('<int:pk>/edit/', login_required(TripUpdateView.as_view()), name='edit'),
    path('<int:pk>/delete/', login_required(TripDeleteView.as_view()), name='delete'),
    path('<int:pk>/signup/', login_required(TripSignUpView.as_view()), name='signup'),
    path('<int:pk>/bail/', login_required(TripBailView.as_view()), name='bail'),
    path('create/', login_required(TripCreateView.as_view()), name='create'),
]
