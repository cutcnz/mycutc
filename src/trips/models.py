from django.db import models
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.contrib.auth.models import User


class Trip(models.Model):
    # Basic
    name = models.CharField(max_length=250)
    description = models.TextField()

    # Date
    start = models.DateField()
    end = models.DateField()

    # Metadata
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('trips:detail', args=[str(self.id)])

    def clean(self):
        if self.end < self.start:
            raise ValidationError({'end': 'The trip probably shouldn\'t end before it starts.'})


class TripParticipant(models.Model):
    # Always linked to user
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='participating_in')

    # Always linked to a trip
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE, related_name='participants')

    # Number of seats in car, assume no car if 0
    seats = models.IntegerField()

    # Any notes to send to the trip leader, medical issues, other things
    notes = models.TextField()

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name
