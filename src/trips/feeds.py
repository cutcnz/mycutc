import datetime

from django.utils import timezone
from django.utils import formats
from django.contrib.syndication.views import Feed
from django.utils.text import Truncator

from .models import Trip

class UpcomingTripsFeed(Feed):
    title = "Upcoming Trips"
    link = "/trips/"
    description = "Upcoming CUTC trips."

    def items(self):
        now = timezone.now()
        return Trip.objects.order_by('end').filter(end__gte=now)[:5]

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        start = formats.date_format(item.start, "DATE_FORMAT")
        return Truncator(("[%s]\n%s" % (start, item.description))).chars(100)

    def item_pubdate(self, item):
        # pubdate is trip start, adds time as datetime object required
        return datetime.datetime.combine(item.start, datetime.datetime.min.time())
