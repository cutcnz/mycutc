from django.utils import timezone
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail

from .models import Trip, TripParticipant
from .forms import TripCreateForm, TripSignupForm, TripBailForm

import logging


class TripsListView(ListView):
    model = Trip
    context_object_name = 'trips'
    template_name = 'trips/index.html'
    queryset = Trip.objects.all().order_by('start')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now = timezone.now()
        context['upcoming_trips'] = self.queryset.filter(end__gte=now)
        context['past_trips'] = self.queryset.filter(end__lt=now)
        return context

class TripDetailView(DetailView):
    model = Trip
    template_name = 'trips/detail.html'

    def get_context_data(self, **kwargs):
        context = super(TripDetailView, self).get_context_data(**kwargs)
        trip = Trip.objects.get(pk=self.kwargs['pk'])
        is_signed = False
        if self.request.user.is_authenticated:
            is_signed = trip.participants.filter(user=self.request.user)
        context['is_signed'] = is_signed
        return context

class TripCreateView(CreateView):
    model = Trip
    form_class = TripCreateForm
    template_name = 'trips/create.html'

    def form_valid(self, form):
        # Set creator to current user
        form.instance.creator = self.request.user
        return super().form_valid(form)

class TripUpdateView(UpdateView):
    model = Trip
    #fields = ('name', 'start', 'end', 'description',)
    form_class = TripCreateForm
    template_name = 'trips/edit.html'

    def form_valid(self, form):
        # Only creator can edit
        if form.instance.creator == self.request.user:
            return super().form_valid(form)
        else:
            raise PermissionDenied


class TripDeleteView(DeleteView):
    model = Trip
    template_name = 'trips/delete.html'
    success_url = reverse_lazy('trips:index')

    def delete(self, request, *args, **kwargs):
        # Only creator can delete
        self.object = self.get_object()

        if self.object.creator == self.request.user:
            return super().delete(self, request)
        else:
            raise PermissionDenied

class TripSignUpView(FormView):
    form_class = TripSignupForm
    template_name = "trips/signup.html"
    success_url = reverse_lazy('trips:index')

    def get_context_data(self, **kwargs):
        context = super(TripSignUpView, self).get_context_data(**kwargs)
        trip = Trip.objects.get(pk=self.kwargs['pk'])
        context['trip'] = trip
        return context

    def form_valid(self, form):
        trip = Trip.objects.get(pk=self.kwargs['pk'])
        seats = int(form.data['number_of_seats_including_yourself'])
        part = TripParticipant(user=self.request.user, trip=trip, seats=seats)
        part.save()

        send_mail('Trip signup',
                  '{} has signed up for your {} trip'.format(part.user.first_name, part.trip.name),
                  'no-reply@cutc.org.nz',
                  [trip.creator.email],
                  fail_silently=False)

        return super().form_valid(form)

class TripBailView(FormView):
    form_class = TripBailForm
    template_name = "trips/bail.html"
    success_url = reverse_lazy('trips:index')

    def get_context_data(self, **kwargs):
        context = super(TripBailView, self).get_context_data(**kwargs)
        trip = Trip.objects.get(pk=self.kwargs['pk'])
        context['trip'] = trip
        return context

    def form_valid(self, form):
        trip = Trip.objects.get(pk=self.kwargs['pk'])
        trip.participants.filter(user=self.request.user).delete()
        return super().form_valid(form)
