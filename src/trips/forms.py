from django import forms

from .models import Trip

class DateInput(forms.DateInput):
    input_type = 'date'

class TripCreateForm(forms.ModelForm):
    class Meta:
        model = Trip
        fields = ['name', 'start', 'end', 'description']
        widgets = {
            'start': DateInput(),
            'end': DateInput()
        }

class TripSignupForm(forms.Form):
    do_you_have_a_car_and_can_drive = forms.BooleanField(required=False)
    number_of_seats_including_yourself = forms.ChoiceField(choices=[('1', '1'),
                                                                    ('2', '2'),
                                                                    ('3', '3'),
                                                                    ('4', '4'),
                                                                    ('5', '5'),
                                                                    ('6', '6'),
                                                                    ('7', '7'),
                                                                    ('8', '8'),
                                                                    ('9', '9'),
                                                                    ('10', '10'),
                                                                    ('11', '11'),
                                                                    ('12', '12)')], required=False)
    i_agree = forms.BooleanField(required=True)


class TripBailForm(forms.Form):
    i_agree = forms.BooleanField(required=False)

