from django.utils import timezone
from django.views.generic import TemplateView

from trips.models import Trip
from meetings.models import Meeting

class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now = timezone.now()
        context['upcoming_trips'] = Trip.objects.filter(end__gte=now).order_by('start')
        context['upcoming_meetings'] = Meeting.objects.filter(when__gte=now).order_by('when')
        return context
