from django.db import models
from django.urls import reverse

class Meeting(models.Model):
    # Basic
    name = models.CharField(max_length=250)
    location = models.CharField(max_length=250)
    description = models.TextField()

    # Date
    when = models.DateTimeField()

    # Metadata
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('meetings:detail', args=[str(self.id)])
