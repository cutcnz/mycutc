import datetime

from django.utils import timezone
from django.utils import formats
from django.contrib.syndication.views import Feed
from django.utils.text import Truncator

from .models import Meeting

class UpcomingMeetingsFeed(Feed):
    title = "Upcoming Meetings"
    link = "/trips/"
    description = "Upcoming CUTC meetings."

    def items(self):
        now = timezone.now()
        return Meeting.objects.order_by('when').filter(when__gte=now)[:5]

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        when = formats.date_format(item.when, "DATETIME_FORMAT")
        return Truncator(("[%s, %s]\n%s" % (when, item.location, item.description))).chars(100)

    def item_pubdate(self, item):
        # pubdate is meeting time
        return item.when
