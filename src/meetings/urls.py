from django.urls import path

from .feeds import UpcomingMeetingsFeed
from .views import MeetingsListView, MeetingDetailView

app_name = 'meetings'

urlpatterns = [
    path('', MeetingsListView.as_view(), name='index'),
    path('feed/', UpcomingMeetingsFeed()),
    path('<pk>/', MeetingDetailView.as_view(), name='detail'),
]
