from django.utils import timezone
from django.views.generic import ListView, DetailView

from .models import Meeting

class MeetingsListView(ListView):
    model = Meeting
    context_object_name = 'meetings'
    template_name = 'meetings/index.html'
    queryset = Meeting.objects.all().order_by('when')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now = timezone.now()
        context['upcoming_meetings'] = self.queryset.filter(when__gte=now)
        context['past_meetings'] = self.queryset.filter(when__lt=now)
        return context

class MeetingDetailView(DetailView):
    model = Meeting
    template_name = 'meetings/detail.html'
