import hashlib
from mailchimp3 import MailChimp

from django.conf import settings

def mailchimp_login():
    """ Login to MailChimp using details from settings """
    try:
        client = MailChimp(settings.MAILCHIMP_API_KEY, settings.MAILCHIMP_USERNAME)
    except:
        pass
    else:
        return client

def get_mailchimp_member(email):
    """ Query MailChimp (lowercase MD5 hash of email) to see if member exists in list """

    try:
        client = mailchimp_login()
        email_hash = hashlib.md5(email.lower().encode('utf-8')).hexdigest()
        member = client.lists.members.get(settings.MAILCHIMP_MEMBERS_LIST, email_hash)
    except:
        return None
    else:
        return member
