from datetime import datetime

from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend
from django.contrib import messages

from .mailchimp import get_mailchimp_member

class MailChimpBackend(ModelBackend):
    """
    Authenticate users against MailChimp:
    * Check email address exists in members database
    * Check today's date is not later than the membership expiry date
    """

    def validate_member(self, request, member):
        """ Ensure member is current """

        if member:
            #messages.add_message(request, messages.INFO, ('%s %s' % (current_date, expiry_date)))

            try:
                current_date = datetime.now().date()
                expiry_date = datetime.now().replace(year=2019)

                # Not everyone has an expiry data set so need to fail gracefully
                try:
                    expiry_date = datetime.strptime(member['merge_fields']['EXPIRY'], "%Y-%m-%d").date()
                except:
                    pass

                for tag in member['tags']:
                    if tag['name'] == 'Signed up for 2021':
                        expiry_date = datetime.now().date().replace(year=2022, month=2, day=15)
            except:
                messages.add_message(request, messages.ERROR, ('Your membership is not yet activated.'))
            else:
                if current_date <= expiry_date:
                    return True
                else:
                    messages.add_message(request, messages.ERROR, ('Your membership expired on %s.' % expiry_date))

        return False

    def mailchimp_authenticate(self, user, request):
        """ Start a MailChimp instance, get member details and validate """

        try:
            member = get_mailchimp_member(user.email)
        except:
            messages.add_message(request, messages.ERROR, 'Failed to connect to membership database.')
        else:
            if self.validate_member(request, member):
                return user

    def django_authenticate(self, request, username, password):
        """ Regular Django authentication """

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            User().set_password(password)
            return User.DoesNotExist
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user

    def authenticate(self, request, username=None, password=None, **kwargs):
        """ Required function, verifying username and password """

        try:
            user = self.django_authenticate(request, username, password)
        except:
            pass
        else:
            # Super-admin does not need validation with MailChimp
            if username == 'cutcnz':
                return user
            elif self.mailchimp_authenticate(user, request):
                return user


    def get_user(self, user_id):
        """ Required function, returning current user """

        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
        return user if self.user_can_authenticate(user) else None
