from mailchimp3 import MailChimp

from django.conf import settings

from .mailchimp import get_mailchimp_member

def get_user_details(user):
    """ Handles obtaining first/last name from MailChimp """

    try:
        member = get_mailchimp_member(user.email)
    except:
        return None
    else:
        return member

def update_user_details(sender, request, user, **kwargs):
    """ Handles the updating of the User object with MailChimp details """

    try:
        member = get_user_details(user)
        if member:
            user.first_name = member['merge_fields']['FNAME']
            user.last_name = member['merge_fields']['LNAME']
            user.save()
    except:
        pass
