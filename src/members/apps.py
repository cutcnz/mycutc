from django.apps import AppConfig
from django.contrib.auth.signals import user_logged_in

from .signals import update_user_details

class MembersConfig(AppConfig):
    name = 'members'

    def ready(self):
        user_logged_in.connect(update_user_details)
